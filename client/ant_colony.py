from graph import graph
from random import uniform
from random import randrange
import subprocess
import socket
import gc
import logging
import sys


class ant_colony:
    """
    Enclosure to start and run a Ant colony
    """

    def __init__(self,problem,N,NC,alpha,beta,rho,t_max,t_min,local_search='lk-opt'):
        """
        Initialize the colony with the required parameters
        :param problem:         Problem to be solved
        :param N:               Number of ants
        :param NC:              Max number of iterations
        :param alpha:           alpha value
        :param beta:            beta value
        :param rho:             rho value
        :param t_max:           Maximum pheromone value
        :param t_min:           Minimum pheromone value
        :param local_search:    local search to be used
        """
        self.raw_problem = problem
        self.problem = graph(problem)
        self.NC = NC
        self.N = N
        self.alpha = alpha
        self.beta = beta
        self.rho = rho
        self.t_max = t_max
        self.t_min = t_min
        self.best_cycle_length=float('inf')
        self.best_cycle = None
        self.state = 'START'
        self.sol_3_opt = {}
        self.dimension = int(self.problem.get_property('DIMENSION'))
        logging.info(local_search)
        if local_search == 'lk-opt':
            self.local_search = self.__lk_opt__
        elif local_search == '2-opt':
            self.local_search = self.__2_opt__
        elif local_search == '3-opt':
            self.local_search = self.__3_opt__
        self.__colony_init__()
        self.NN_list={}


    def __colony_init__(self):
        """
        Initialize the colony by creating the required data structures
        :return: None
        """
        if self.state == 'START':
            self.state = 'INIT'


    def get_probability(self,x,y):
        """
        Get the probability of selecting the edge (x,y)
        :param x:   x vertex
        :param y:   y vertex
        :return:    probability of selecting the edge (x,y)
        """
        return (100/self.problem.get_length(x, y)) ** self.beta + (self.problem.get_phermone(x, y) ** self.alpha)


    def ant_colony_run(self):
        """
        Main loop of the ant colony
        :return:
        """
        logging.info('Starting Ant colony optimization')
        self.state = 'RUN'
        for i in range(self.NC):
            if self.state == 'RUN':
                self.tabu = [[randrange(0, self.dimension)] for y in range(self.N)]
                for y in range(self.dimension):
                    for x in range(self.N):
                        self.tabu[x].append(self.__calculate_next_edge__(self.tabu[x][y], x))
                len = self.__estimate_path_lengths__()
                """Update the phermone with best path"""
                smallest_cycle = min(len)
                shortest_path = len.index(smallest_cycle)
                gc.collect()
                self.tabu[shortest_path] = self.local_search(self.tabu[shortest_path], smallest_cycle)
                len = self.__estimate_path_lengths__()
                smallest_cycle = len[shortest_path]
                logging.info('Found path of cost '+str(smallest_cycle))
                if self.best_cycle_length > smallest_cycle:
                    self.best_cycle_length = smallest_cycle
                    self.best_cycle = self.tabu[shortest_path]
                for x in range(self.dimension):
                    for y in range(self.dimension):
                        if x!=y:
                            self.problem.set_phermone(x,y, self.problem.get_phermone(x,y)*self.rho)
                del self.tabu
        return self.best_cycle, self.best_cycle_length




    def __calculate_next_edge__(self,x,i):
        """
        Select the next edge to be used
        :param x:   vertex to find next edge from
        :param i:   the id of the ant
        :return:    choice of the next vertex
        """
        prob=[(y,self.get_probability(x,y)) for y in range(self.dimension) if y not in self.tabu[i] and self.problem.get_length(x,y)!=0 ]
        if prob.__len__()==0:
            return self.tabu[i][0]
        choice=uniform(0,prob[prob.__len__()-1][1])
        for y in prob:
            k,j=y
            if choice<j:
                return k

    def __estimate_path_lengths__(self):
        """
        Estimate the cycle cost of all the cycles
        :return:    List containing the cost of all cycles
        """
        lengths=[]
        for x in self.tabu:
            lengths.append(sum(self.problem.get_length(x[i],x[i+1]) for i in range(x.__len__()-1)))
        return lengths

    def __update__params__(self,alpha=None, beta=None, rho=None, NC=None, N=None, t_max=None, t_min=None):
        """
        Update the parameters
        :param alpha:
        :param beta:
        :param rho:
        :param NC:
        :param N:
        :param t_max:
        :param t_min:
        :return:
        """
        if alpha == None: self.alpha = alpha
        if beta == None: self.beta = beta
        if rho == None: self.rho = rho
        if NC == None: self.NC = NC
        if N == None: self.N = N
        if t_min == None: self.t_min = t_min
        if t_max == None: self.t_max = t_max

    def __update_pheromone__(self,pheromone):
        """
        Update the pheromone
        :param pheromone:   list containing pheromone
        :return:
        """
        if(pheromone!=None):
            for entry in pheromone:
                element = entry.split()
                # print(element)
                if element.__len__() == 3:
                    a = int(element[0])
                    b = int(element[1])
                    val = float(element[2])
                    val = 10 if val > 10 else 0.5 if val < 0.5 else val
                    self.problem.set_phermone(a, b, self.problem.get_phermone(a,b)+val)
                    self.problem.set_phermone(b, a, self.problem.get_phermone(b,a)+val)

    def __2_opt__(self,path,len):
        """
        Run 2-opt on the path
        :param path:
        :param len:
        :return:
        """
        running = True
        prev_len = len
        while running:
            running = False
            # for target in range(self.tabu.__len__()):
            for idx1 in range(path.__len__() - 2):
                a = path[idx1]
                b = path[idx1 + 1]
                present_edge_cost = self.problem.get_length(b, a)
                gain = 0
                gain_max = None
                for idx2 in range(idx1 + 2, path.__len__() - 1):
                    c = path[idx2]
                    d = path[idx2 + 1]
                    trail_edge = self.problem.get_length(c, d)
                    if ((present_edge_cost + trail_edge) - (
                        self.problem.get_length(a, c) + self.problem.get_length(d, b))) > gain:
                        gain = ((present_edge_cost + trail_edge) - (
                        self.problem.get_length(a, c) + self.problem.get_length(d, b)))
                        # print(gain)
                        gain_max = idx2
                        break
                if gain_max != None:
                    running = True
                    path = path[:(idx1 + 1)] + list(reversed(path[(idx1 + 1):gain_max + 1])) + path[gain_max + 1:]
            tmp_len = self.__estimate_path_lengths__()
            prev_len = tmp_len
        return path

    def __lk_opt__(self,path,len):
        """
        Run lk optimization
        :param path:
        :param len:
        :return:
        """
        name = socket.gethostname()
        op_file=open('lkh/'+name+'_problem.tsp','w')
        op_file.write(self.raw_problem)
        op_file.close()
        tour_file = open('lkh/'+name+'_problem.tour','w')
        tour_file.write('TYPE : TOUR\n')
        tour_file.write('DIMENSION :'+str(self.dimension)+'\n')
        tour_file.write('TOUR_SECTION\n')
        for i in range(path.__len__()):
            if i != path.__len__()-1:
                tour_file.write(str(path[i]+1)+'\n')
        tour_file.write('-1')
        tour_file.close()
        par_file = open('lkh/'+name+'_problem.par','w')
        par_file.write('PROBLEM_FILE = lkh/'+name+'_problem.tsp\n')
        par_file.write('INITIAL_TOUR_FILE = lkh/'+name+'_problem.tour\n')
        par_file.write('OUTPUT_TOUR_FILE = lkh/'+name+'_problem.opt.tour\n')
        par_file.write('CANDIDATE_SET_TYPE = NEAREST-NEIGHBOR\n')
        par_file.write('MAX_TRIALS = '+str(self.dimension//10)+'\n')
        par_file.write('RUNS = 1\n')
        par_file.close()
        subprocess.check_call(['lkh/LKH', 'lkh/'+name+'_problem.par'])
        tour_file = open('lkh/'+name+'_problem.opt.tour')
        tour = tour_file.read()
        tour = tour.split('TOUR_SECTION')[1]
        tour = [int(x)-1 for x in tour.split() if x != '-1' and x !='EOF'and x != '']
        tour.append(tour[0])
        # print('Done Local Optimization')
        return tour


    def __3_opt__(self, path_to_improve, len):
        """
        Run 3-opt
        :param path_to_improve:
        :param len:
        :return:
        """
        running = True
        flips=[]
        flips_direction=[]
        path = path_to_improve
        while running:
            running = False
            for i in range(path.__len__()-1):
                a = path[i]
                b = path[i+1]
                gain = 0
                a_b = self.problem.get_length(a,b)
                for j in self.__get__nearest__neighbors__(i):
                    c = path[j]
                    d = path[j+1]
                    c_d = self.problem.get_length(c,d)
                    d1 = a_b+c_d
                    d2 = self.problem.get_length(a, c) + self.problem.get_length(b,d)
                    if d1 > d2 and a != c and a != d and b != c and b != d:
                        #perform 2-opt
                        gain = d1 - d2
                        flips = [i,j,i+1,j+1]
                        flips_direction = [False,True]
                    if self.problem.get_length(a,c) < a_b or self.problem.get_length(b,d) < a_b:
                        for k in self.__get__nearest__neighbors__(j):
                            e = path[k]
                            f = path[k+1]
                            if (a_b+c_d) > (self.problem.get_length(a,d)+self.problem.get_length(c,e)) or (a_b+c_d) > (self.problem.get_length(a,c)+self.problem.get_length(d,f)):
                                d1 = self.problem.get_length(a,b) + self.problem.get_length(d,c) + self.problem.get_length(f,e)
                                d2 = self.problem.get_length(a,d) + self.problem.get_length(c,e) + self.problem.get_length(b,f)
                                if d1-d2 > gain and self.__is_valid_path__(path,[i,j+1,k,j,i+1,k+1],[True,False,True]):
                                    gain = d1-d2
                                    flips=[i,j+1,k,j,i+1,k+1]
                                    flips_direction = [True,False,True]
                                    break
                                    # d1 = self.problem.get_length(a,b) + self.problem.get_length(d,c) + self.problem.get_length(f,e)
                                d2 = self.problem.get_length(a,d) + self.problem.get_length(b,e) + self.problem.get_length(c,f)
                                if d1-d2 > gain and self.__is_valid_path__(path,[i,j+1,k,i+1,j,k+1],[True, True, True]):
                                    gain = d1 - d2
                                    flips=[i,j+1,k,i+1,j,k+1]
                                    flips_direction = [True, True, True]
                                    break
                            else:
                                break
                        if gain > 0:
                            break
                    else:
                        break
                if gain > 0:
                    len -= gain
                    path = self.__reconnect_path__(path,flips,flips_direction)
                    # print('Improving by ' + str(gain) + ' to ' + str(len)+ ' ' + str(path.__len__()))
                    if len not in self.sol_3_opt:
                        # Avoid previously found paths from running the check again
                        running = True
            if len not in self.sol_3_opt:
                self.sol_3_opt[len] = path
        return path


    def __length_of_flip__(self,path,i,j,direction):
        """
        The final length of the flip
        :param path:
        :param i:
        :param j:
        :param direction:
        :return:
        """
        if j >= i and direction:
            return j-i+1 #Inclusive ??
        elif j >=i :
            return path.__len__() - (j-i)
        elif i > j and direction:
            return path.__len__() - (i-j)
        else:
            return i-j+1

    def __is_valid_path__(self,path,flips,direction):
        """
        Check if the path is valid
        :param path:
        :param flips:
        :param direction:
        :return:
        """
        length=1
        for i in range(flips.__len__() // 2):
            length+=self.__length_of_flip__(path,flips[(2 * i) + 1], flips[2 * (i + 1) % flips.__len__()], direction[i])
        return length == path.__len__()

    def __get_path_from_i_to_j__(self,path,i,j, direction):
        """
        Get path from i to j after flipping the direction
        :param path:
        :param i:
        :param j:
        :param direction:
        :return:
        """
        if j >= i and direction:
            return path[i:j+1] #Inclusive ??
        elif j >=i :
            return path[:i+1][::-1]+path[j:-1][::-1]
        elif i > j and direction:
            return path[i:] + path[1:j+1]
        else:
            return path[j:i+1][::-1]

    def __reconnect_path__(self, path, flips, direction):
        """
        Reconnect the path
        :param path:
        :param flips:
        :param direction:
        :return:
        """
        new_path = []
        new_path.append(path[flips[0]])
        for i in range(flips.__len__()//2):
            new_path += self.__get_path_from_i_to_j__(path,flips[(2*i)+1],flips[2*(i+1)%flips.__len__()],direction[i])
        return new_path

    def __get__nearest__neighbors__(self,i):
        """
        Get the nearest neighbours
        :param i:
        :return:
        """
        if i not in self.NN_list:
            self.NN_list[i] = sorted([x for x in range(self.dimension)],key=lambda x: self.problem.get_length(i,x))#[:40]
        return self.NN_list[i]

