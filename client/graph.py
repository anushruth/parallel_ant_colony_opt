import math
from random import randrange
class graph:


    def __init__(self, problem):
        """
        Initialize the instance with the name of the problem
        Make sure that the problem is in 'pwd'/data/TSP/<problem-name>.tsp
        """

        content=problem.splitlines()
        cleaned=[x.lstrip() for x in content if x !=""]
        self.load_graph(cleaned)
        self.phermone = [[0 for x in range(int(self.get_property('DIMENSION')))] for y in range(int(self.get_property('DIMENSION')))]
        self.distance = [[None for x in range(int(self.get_property('DIMENSION')))] for y in
                         range(int(self.get_property('DIMENSION')))]

    def load_graph(self,graph):
        """
        Load the graph parameters into the instance
        :param graph: Raw graph loaded as a list of strings
        :return:  None
        """
        self.raw_graph_params={x.split(':')[0].strip():x.split(':')[1].strip() for x in graph if ':' in x}
        self.load_data(graph)

    def load_data(self,data):
        """
        Load the data associated for the graph (vertices and edge cost)
        based on the type of representation
        :param data: Raw graph as a list of strings
        :return: None
        """
        if self.raw_graph_params['EDGE_WEIGHT_TYPE'] == 'EUC_2D':
            self.raw_graph={int(x.split()[0].strip())-1:(float(x.split()[1].strip()), float(x.split()[2].strip())) for x in data if len(x.split())>2 and ':' not in x}

    def get_length(self,x,y):
        """
        Returns the length between x and y in a graph based on the type of graph
        :param x: Start point
        :param y: End point
        :return: Distance between the start and end points
        """
        ## change to support different representations
        if self.distance[x][y] == None:
            self.distance[x][y] = int(math.sqrt(((self.raw_graph[x][0]-self.raw_graph[y][0])**2 + (self.raw_graph[x][1]-self.raw_graph[y][1])**2))+0.5)
        return self.distance[x][y]

    def get_property(self,property='NAME'):
        """
        Returns the requested property of the graph
        :param property: Property value to be returned
        :return: Property value
        """
        return self.raw_graph_params[property]

    def set_phermone(self,x,y,strength=0):
        """
        Set the phermone level
        :param location:
        :param strength:
        :return:
        """
        if self.phermone[x][y] == 0:
            self.phermone[x][y] = randrange(0,1)
        self.phermone[x][y] = strength

    def get_phermone(self,x,y):
        """
        get the phermone level
        :param x:
        :param y:
        :return:
        """
        return self.phermone[x][y]

    def get_cycle_len(self,cycle):
        length = 0
        for i in range(cycle.__len__()-1):
            length += self.get_length(cycle[i],cycle[i+1])
        return length