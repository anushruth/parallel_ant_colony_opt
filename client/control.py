from ant_colony import ant_colony
import socket
import sys
import time
import errno
import logging


class control:
    """
    Runs the main control for the client
    """

    def __init__(self,server_address,server_port,name=None):
        """
        Initialize the Client with the server address and server_port
        :param server_address:  server-address
        :param server_port:     port number
        """
        self.state = 'Init'
        self.master = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.master.connect((server_address, server_port))
        if name==None:
            self.master.send(bytearray(socket.gethostname().encode()))
        else:
            self.master.send(name)
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.DEBUG)
        logging.info('Initialized Control')

    def run(self):
        """
        The main loop of the client
        :return:
        """
        if self.recv() == 'OK':
            self.problem = self.recv()
            N, NC, alpha, beta, rho, t_max, t_min, local_search = self.recv_params()
            self.colony = ant_colony(self.problem, N, NC, alpha, beta, rho, t_max, t_min, local_search)
            cycle, cycle_len = self.colony.ant_colony_run()
            self.send_best_cycle(cycle, cycle_len)
            while self.state != 'END':
                pheromone = self.recv_pheromone()
                if not pheromone:
                    return
                params = self.recv_params()
                if params == None:
                    return
                self.state = 'RUN'
                cycle, cycle_len = self.colony.ant_colony_run()
                self.send_best_cycle(cycle, cycle_len)


    def recv_params(self):
        """
        Code to receive the necessary params to run the colony
        :return: All the necessary parameters
        """
        N = None
        NC = None
        alpha = None
        beta = None
        rho = None
        t_max = None
        t_min = None
        local_search = None
        while N == None or NC == None or alpha == None or beta == None or rho == None or t_min == None or t_max == None or local_search == None:
            data = self.recv()
            if data == None:
                return None
            tmp = data.split()
            if tmp[0].strip() == 'N':
                N = int(tmp[1])
            elif tmp[0].strip() == 'NC':
                NC = int(tmp[1])
            elif tmp[0].strip() == 'ALPHA':
                alpha = float(tmp[1])
            elif tmp[0].strip() == 'BETA':
                beta = float(tmp[1])
            elif tmp[0].strip() == 'RHO':
                rho = float(tmp[1])
            elif tmp[0].strip() == 'T_MAX':
                t_max = float(tmp[1])
            elif tmp[0].strip() == 'T_MIN':
                t_min = float(tmp[1])
            elif tmp[0].strip() == 'LOCAL_SEARCH':
                local_search = tmp[1]
            else:
                return None
        return  N,NC,alpha,beta,rho,t_max,t_min,local_search

    def recv(self):
        """
        Receive a message from the server
        :return:    Message
        """
        data = 'WAIT'
        while data == 'WAIT':
            tmp = ''
            msg_len_desc_len = 8
            while msg_len_desc_len > 0:
                chunk = self.master.recv(msg_len_desc_len)
                tmp = tmp+str(chunk.decode())
                msg_len_desc_len = msg_len_desc_len-chunk.__len__()
            len = int(tmp)
            recd_bytes = 0
            recd_data = []
            while recd_bytes < len:
                chunk = self.master.recv(len - recd_bytes)
                if chunk != b'' or chunk != None:
                    recd_data.append(chunk)
                    recd_bytes += chunk.__len__()
            data = ''
            for bt in recd_data:
                data = data+str(bt.decode())
            # print('Received '+data)
        if data == 'STOP':
            self.state = 'END'
            return None
        return data

    def recv_pheromone(self):
        """
        Receive the pheromone update values from the server
        :return:
        """
        data = self.recv()
        if data != None and 'PHEROMONE_MATRIX' in data:
            data = data.strip('PHEROMONE_MATRIX')
            data = data.split('\n')
            self.colony.__update_pheromone__(data)
            return True
        else:
            return False

    def send(self,msg):
        """
        Send a message to the server
        :param msg:     Message
        :return:
        """
        data = bytearray(msg.encode())
        header =  '{0:08}'.format(data.__len__())
        message = header + msg
        self.master.sendall(bytearray(message.encode()))

    def send_best_cycle(self,cycle,cycle_len):
        """
        Send the best cycle back to the server
        :param cycle:       list containing the cycle
        :param cycle_len:   cost of the cycle
        :return:
        """
        data = 'BEST_CYCLE '
        for point in cycle:
            data = data + ' ' + str(point)
        self.send(data)
        data = 'BEST_CYCLE_LEN '+str(cycle_len)
        self.send(data)


def print_usage():
    print('Usage: python3.6 control.py server_address:port [Client-identifier]')

if sys.argv.__len__() < 2:
    print('Error')
    print_usage()
    exit(-1)

client_name=None
server_address = sys.argv[1].split(':')
server_port = int(server_address[1])
server_address = server_address[0]
if sys.argv.__len__() == 3:
    client_name = sys.argv[2]


err_count = 100
while True:
    time.sleep(1)
    try:
        control(server_address,server_port,client_name).run()
    except OSError as e:
        if err_count>0:
            logging.error('Error['+str(e.errno)+']: '+e.strerror)
            logging.error('Unable to connect to '+server_address+':'+str(server_port))
            logging.INFO('Attempting again')
            err_count-=1
        else:
            break

