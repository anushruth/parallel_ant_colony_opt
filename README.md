# README #

Parallel Implementation of ant colony algorithm for Travelling Salesman Problem

### What is this repository for? ###

* Mainly a container for the code that was used to experiment

### Requirements ###
* python3.6

###Instructions for running ###

* Install copies of code in both server and client
* Use git to install the copies 
    > git clone https://bitbucket.org/anushruth/parallel_ant_colony_opt.git
* Compile lkh code in lkh directory using make

### Running Server ###

python3.6 server/master.py [options] <problem-name>

Running the program without any problem name shows the list of available options

ex: python3 -u server/master.py pcb442 -s -t=56892 -p="(10,5,5,0.8,100,1)"

Exit the program using Ctrl+C after the results are displayed

### Running Client ###

python3.6 client/control.py server_address:port client_name

client_name can be any identifier. Not required if multiple computers are used as clients.
If the same computer is used to run multiple processes of clients then this is required

