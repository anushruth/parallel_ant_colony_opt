from parallel_ant_colony import parallel_ant_colony
from client_handler import client_handler
import socketserver
import sys
import threading
import logging

sync = False
problem = None
solution = 0
start_point = 1
end_point = 2
step_size = 1
number_of_runs = 20
local_search = 'lk-opt'
port = 8888
params = (1,10,5,5,0.8,100,1)

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.DEBUG)

def print_usage():
    print('Usage: python3.6 master.py [Options] problem')
    print('\nOptions:')
    print('             -s or --sync                Run synchronous ant colony optimization')
    print('             -t                          Limiting point or stopping point for the algorithm (cost of the solution)')
    print('             -start=<number-of-nodes>    Starting number of nodes')
    print('             -stop=<number-of-nodes>     Run until')
    print('             -inc=<number-of-nodes>      Increment after every run')
    print('             -runs=<number-of-runs>      Number of runs for each configuration')
    print('             -l=<local-search>           The local search, [2-opt 3-opt lk-opt]')
    print('             -p=(parameters)             (number-of-ants,alpha,beta,gamma,t_max,t_min)')
    print('\nProblem instances are searched for in TSPLIB database in \'data\' folder')


if(sys.argv.__len__()<3):
    print_usage()
    exit(-1)

for arg in sys.argv:
    try:
        if arg == '-s' or arg == '--sync':
            sync = True
        elif arg.startswith('-t='):
            solution = float(arg.strip('-t='))
        elif arg.startswith('-start='):
            start_point = int(arg.strip('-start='))
        elif arg.startswith('-stop='):
            end_point = int(arg.strip('-stop='))
        elif arg.startswith('-inc='):
            step_size = int(arg.strip('-inc='))
        elif arg.startswith('-runs='):
            number_of_runs = int(arg.strip('-runs='))
        elif arg.startswith('-l='):
            local_search = arg.strip('-l=')
        elif arg.startswith('-p='):
            tmp = arg.strip('-p=').strip(')').strip('(').split(',')
            params = (1,int(tmp[0]),float(tmp[1]),float(tmp[2]),float(tmp[3]),float(tmp[4]),float(tmp[5]))
        else:
            problem = arg
    except ValueError as e:
        logging.error('Invalid value input '+e.__str__())
        print_usage()
        exit(-1)

avg_time=[0]*(end_point-start_point)
avg_iter=[0]*(end_point-start_point)

update_lock = threading.Lock()
handler_lock = threading.Lock()

def update_data(size,time,iterations):
    global avg_time
    global avg_iter
    global clnt_count
    update_lock.acquire(blocking=True)
    logging.info('Time['+str(size)+']: '+str(time))
    avg_time[size-start_point] += time/number_of_runs
    avg_iter[size-start_point] += iterations/number_of_runs
    update_lock.release()
    clnt_count-=1
    if clnt_count == 0:
        print('The average time is:')
        print(avg_time)
        # print(avg_iter)

node_len = start_point

lck = threading.Lock()
handler_count = node_len
runs = number_of_runs
clnt_handler = client_handler(problem,parallel_ant_colony(params),node_len,update_data,local_search,sync,solution)
clnt_count = 1
stop = False

def get_client_handler():
    global handler_count
    global clnt_count
    global clnt_handler
    global node_len
    global runs
    global stop
    handler_lock.acquire(blocking=True)
    if stop:
        return None
    if handler_count == 0:
        if runs == 0:
            node_len += step_size
            runs = number_of_runs
        if node_len == end_point:
            stop = True
            return None
        handler_count = node_len
        handler_count-=1
        runs-=1
        logging.info('Handling problem of size ' + str(node_len))
        clnt_handler = client_handler(problem,parallel_ant_colony(params),node_len,update_data,local_search,sync,solution)
        clnt_count+=1
        handler_lock.release()
        return clnt_handler
    else:
        handler_count-=1
        handler_lock.release()
        return clnt_handler





class TCPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        global handler_count
        handler = get_client_handler()
        data=''
        if handler == None:
            logging.warning('Done with all the handlers, No request will be accepted')
            return
        try:
            data = self.request.recv(1024)
            handler.__handle_client__(str(data.decode()),self.request)
        except UnicodeDecodeError as e:
            handler_count+=1
            logging.error('Illegal request:['+self.request+str(data))




socketserver.TCPServer.allow_reuse_address = True
logging.info('Starting the Server on port '+str(port))

with socketserver.ThreadingTCPServer(('', port), TCPHandler,bind_and_activate=True) as server:
    server.serve_forever()


