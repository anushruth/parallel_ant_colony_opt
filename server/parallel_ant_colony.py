from threading import Lock
from random import randrange
from random import uniform

class parallel_ant_colony:

    def __init__(self,params=(1,10,5,5,0.8,100,1)):
        self.subcolonies = {}
        self.best_cycles = {}
        self.best_cycle_lens ={}
        self.best_cycle_len = float('inf')
        self.best_cycle = None
        self.params = params
        self.lock = Lock()

    def get_params(self,colony):
        self.lock.acquire(blocking=True)
        params =  self.subcolonies[colony]
        self.lock.release()
        return params


    def add_colony(self,name):
        self.lock.acquire(blocking=True)
        self.subcolonies[name] = self.params
        self.lock.release()
        self.best_cycle_lens[name] = float('inf')
        self.best_cycles[name] = None

    def update_cycle(self,name,cycle,cycle_len):
        self.lock.acquire(blocking=True)
        # if self.best_cycle_lens[name] > cycle_len:
        self.best_cycles[name] = cycle
        self.best_cycle_lens[name] = cycle_len
        if self.best_cycle_len > cycle_len:
            self.best_cycle_len = cycle_len
            self.best_cycle = cycle
            # print('Best Cycle : [' + str(self.best_cycle_len) +'] ')
        self.lock.release()

    def get_pheromone_matrix(self):
        self.lock.acquire(blocking=True)
        pheromone_matrix = {}
        # print('Pheromone Matrix:')
        for cycle in  self.best_cycles:
            if self.best_cycles[cycle] != None:
                path = self.best_cycles[cycle].split()
                for idx in range(path.__len__()-2):
                    index = str(path[idx].strip()) + ' ' + str(path[idx + 1].strip())
                    if index in pheromone_matrix:
                        pheromone_matrix[index] += 100000/self.best_cycle_lens[cycle]
                    else :
                        pheromone_matrix[index] = 100000/self.best_cycle_lens[cycle]
        self.lock.release()
        # print(pheromone_matrix)
        return pheromone_matrix