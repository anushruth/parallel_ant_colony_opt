from server.parallel_ant_colony import parallel_ant_colony
from client.graph import graph
import select
import threading
import time
import timeit
import logging
from functools import reduce

class client_handler:
    """
    Handles the connected clients
    """
    def __init__(self,problem,colony,node_count,res_call_back,local_search,synchronous=True,solution=0):
        """
        Initialization block for the client handler
        :param problem:     The name of the problem to be solved
        :param colony:      The colony object to keep track of all the clients
        :param node_count:  Expected number of nodes to run on the this run
        :res_call_back:     Call Back function used to update the results after the run
        :synchronous:       If True the synchronous version of the algorithm is run
        :solution:          The stopping point for the solution
        """
        self.client_list={}
        self.colony = colony
        self.stop = False
        self.sync_lock = threading.Lock()
        self.client_list_lock = threading.Lock()
        self.iter_counter_lock = threading.Lock()
        self.start_event = threading.Event()
        self.stop_event = threading.Event()
        self.solution = solution
        self.node_count = node_count
        self.synchronous=synchronous
        self.call_back = res_call_back
        self.iter_counter = 0
        self.local_search = local_search
        with open('data/' + problem + '.tsp') as prb:
            self.problem = prb.read()
        threading.Thread(target=self.control_thread).start()

    def control_thread(self):
        """
        Handles all the control in the server
        :return:
        """
        # Wait until all the nodes join and then trigger start event
        while self.__node_count__() != self.node_count:
            time.sleep(1)
            continue
        #Initialize the sync event in case of synchronous run
        if self.synchronous:
            self.sync_event = threading.Event()
        #Start the timer after all the clients join the server
        tic = timeit.default_timer()
        #Set the start event to signal all the clients
        logging.info('Starting the run')
        self.start_event.set()
        if self.synchronous:
            # If synchronous, this is the sync loop for all the clients
            while not self.stop:
                self.client_list_lock.acquire(blocking=True)
                events = [self.client_list[client][1] for client in self.client_list]
                self.client_list_lock.release()
                for event in events:
                    event.wait()
                for event in events:
                    event.clear()
                self.sync_event.set()
                self.sync_event.clear()
            self.sync_event.set()
        while not self.stop:
            time.sleep(1)
            continue
        self.__cleanup_and_send_stop__()
        self.stop_event.set()
        run_time = timeit.default_timer() - tic
        self.call_back(self.node_count,run_time,self.__get_iter_count__())
        # logging.info('Time taken '+ str(run_time)+' iterations '+str(self.__get_iter_count__()) + ' size '+str(self.node_count))



    def __register_client__(self,client_name,client_socket,client_event):
        """
        Attempt to register a client
        :param client_name:     Identifier of the client
        :param client_socket:   Socket of the client
        :param client_event:    An event object to synchronize the client
        :return:                True if registration is successful False otherwise
        """
        self.client_list_lock.acquire(blocking=True)
        if self.client_list.__len__() == self.node_count:
            self.__send__('END',client_socket)
            self.client_list_lock.release()
            return False
        if client_name in self.client_list:
            try:
                client_socket_old, client_event_old = self.client_list[client_name]
                client_socket_old.close()
            except ConnectionError:
                logging.error('Old connection to ' + client_name + ' dropped')
        self.client_list[client_name] = (client_socket, client_event)
        self.colony.add_colony(client_name)
        self.client_list_lock.release()
        return True


    def __handle_client__(self, client_name, client_socket):
        """
        Main client loop
        :param client_name:     Identifier of the client
        :param client_socket:   Client socket
        :return:
        """
        ## Try to register the node
        client_event = threading.Event()
        if not self.__register_client__(client_name, client_socket,client_event):
            return
        # Wait until all the nodes are in
        while True:
            self.start_event.wait(5)
            if self.start_event.is_set():
                break
            else:
                self.__send__('WAIT',client_socket)
        self.__send__('OK',client_socket)
        self.__send__problem__(client_socket)
        while not self.stop :
            self.__send_params__(client_name)
            logging.info(client_name+': waiting for best cycle')
            best_cycle = self.__recv_best_cycle__(client_socket)
            self.__update_iter__counter()
            if best_cycle == None or self.stop:
                break
            if float(best_cycle[1]) - self.solution < 0.0001:
                self.stop = True
                client_event.set()
                self.__stop_thread__()
                return
            self.colony.update_cycle(client_name, best_cycle[0], float(best_cycle[1]))
            if self.synchronous and not self.stop:
                client_event.set()
                self.sync_event.wait()
            if self.stop:
                client_event.set()
                self.__stop_thread__()
                return
            else:
                self.__send_pheromone__(client_socket)
        if self.synchronous:
            client_event.set()
        self.__stop_thread__()

    def __node_count__(self):
        """
        Count and return the number of clients handled
        :return:    number of clients
        """
        self.client_list_lock.acquire(blocking=True)
        count = self.client_list.__len__()
        self.client_list_lock.release()
        return count

    def __send__(self, message, client_socket):
        """
        Send the message to the client
        :param message:         Message to be sent
        :param client_socket:   Socket
        :return:
        """
        data = bytearray(message.encode())
        client_socket.setblocking(1)
        client_socket.sendall(bytearray('{0:08}'.format(data.__len__()).encode()))
        client_socket.sendall(data)

    def __recv__(self, client_socket):
        """
        Receive message from client socket
        :param client_socket:
        :return:
        """
        client_socket.setblocking(1)
        tmp = ''
        msg_len_desc_len = 8
        try:
            while self.stop != True:
                if self.stop:
                    return  None
                if client_socket and not self.stop:
                    data = client_socket.recv(msg_len_desc_len)
                    if data:
                        tmp = tmp + str(data.decode())
                        msg_len_desc_len = msg_len_desc_len - data.__len__()
                        break
        except OSError as e:
            if self.stop:
                return None
            else:
                logging.error(e.strerror)
        if self.stop:
            return None
        while msg_len_desc_len > 0:
            chunk = client_socket.recv(msg_len_desc_len)
            tmp = tmp + str(chunk.decode())
            msg_len_desc_len = msg_len_desc_len - chunk.__len__()
        len=int(tmp)
        recd_bytes = 0
        recd_data = ''
        while recd_bytes < len:
            chunk = client_socket.recv(len - recd_bytes)
            if chunk != b'' or chunk != None:
                recd_data = recd_data + str(chunk.decode())
                recd_bytes += chunk.__len__()
        return recd_data

    def __recv_best_cycle__(self,client):
        """
        Receive best cycle from the client
        :param client:  client socket
        :return:        Best cycle and cycle length received
        """
        data = self.__recv__(client)
        if data == None:
            return None
        if 'BEST_CYCLE' in data:
            cycle = data.strip('BEST_CYCLE')
        else: return None
        data = self.__recv__(client)
        if data != None and 'BEST_CYCLE_LEN' in data:
            cycle_len = data.strip('BEST_CYCLE_LEN')
        else: return  None
        return cycle, cycle_len

    def __send__problem__(self,client):
        """
        Send the problem to the client
        :param client:  Client socket
        :return:
        """
        self.__send__(self.problem,client)

    def __send_pheromone__(self,client):
        """
        Send the updated pheromone to the client
        :param client:  client socket
        :return:
        """
        matrix = self.colony.get_pheromone_matrix()
        data = 'PHEROMONE_MATRIX\n'
        for entry in matrix:
            data = data + entry + ' ' + str(matrix[entry]) + '\n'
        self.__send__(data,client)

    def __get_client_socket__(self, client_name):
        """
        Get socket of the client
        :param client_name:     client name
        :return:
        """
        self.client_list_lock.acquire(blocking=True)
        client_socket, count = self.client_list[client_name]
        self.client_list_lock.release()
        return client_socket


    def __send_params__(self,client):
        """
        send the parameters to the client
        :param client:  client socket
        :return:
        """
        NC,N,alpha,beta,rho,t_max,t_min = self.colony.get_params(client)
        client_socket = self.__get_client_socket__(client)
        self.__send__('NC ' + str(NC),client_socket)
        self.__send__('N ' + str(N),client_socket)
        self.__send__('ALPHA ' + str(alpha),client_socket)
        self.__send__('BETA ' + str(beta), client_socket)
        self.__send__('RHO ' + str(rho), client_socket)
        self.__send__('T_MAX '+str(t_max),client_socket)
        self.__send__('T_MIN '+str(t_min),client_socket)
        self.__send__('LOCAL_SEARCH '+str(self.local_search),client_socket)


    def __cleanup_and_send_stop__(self):
        """
        Send stop to all clients and close the socket
        :return:
        """
        self.client_list_lock.acquire(blocking=True)
        for client in self.client_list:
            client_socket, count = self.client_list[client]
            self.__send__('STOP',client_socket)
            client_socket.close()
        self.client_list_lock.release()

    def __stop_thread__(self):
        """
        Wait on the stop event to stop all the clients
        :return:
        """
        self.stop_event.wait()
        return

    def __update_iter__counter(self):
        """
        Update iterations counter
        :return:
        """
        self.iter_counter_lock.acquire(blocking=True)
        self.iter_counter+=1
        self.iter_counter_lock.release()

    def __get_iter_count__(self):
        """
        get iteration counter value
        :return:    iteration counter value
        """
        self.iter_counter_lock.acquire(blocking=True)
        count = self.iter_counter
        self.iter_counter_lock.release()
        return count